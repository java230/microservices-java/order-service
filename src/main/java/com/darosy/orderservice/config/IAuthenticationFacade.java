package com.darosy.orderservice.config;

import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

public interface IAuthenticationFacade {
    JwtAuthenticationToken getAuthentication();
}
