package com.darosy.orderservice.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class AuthenticationFacade implements IAuthenticationFacade {

    @Override
    public JwtAuthenticationToken getAuthentication() {
        return (JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
    }

    private Map<String, Claim> getAdditionalDetail() {
        DecodedJWT jwt = JWT.decode(getToken());
        return jwt.getClaims();
    }

    public String getToken() {
        JwtAuthenticationToken authentication = getAuthentication();
        if (authentication == null)
            return null;
        return authentication.getToken().getTokenValue();
    }

    public String getUserId() {
        Authentication authentication = getAuthentication();
        if (authentication == null)
            return null;
        return authentication.getPrincipal() != null ? authentication.getPrincipal().toString() : "SYSTEM";
    }

    public String getUserName() {
        return getAdditionalDetail().get("preferred_username").as(String.class);
    }
}
