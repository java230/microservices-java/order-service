package com.darosy.orderservice.service;

import com.darosy.orderservice.client.InventoryServiceClient;
import com.darosy.orderservice.dto.OrderRequest;
import com.darosy.orderservice.model.Order;
import com.darosy.orderservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {
    private final OrderRepository orderRepository;
    private final InventoryServiceClient inventoryServiceClient;
    private final StreamBridge streamBridge;

    @Transactional
    public String placeOrder(OrderRequest orderRequest) {
        boolean productStockInStock = orderRequest.getOrderLineItems().stream()
                .allMatch(orderLineItems -> inventoryServiceClient.isInStock(orderLineItems.getSkuCode()));
        if (!productStockInStock) {
            return "Order Failed, One of the product in the order is not in stock";
        }

        Order order = Order.builder()
                .orderLineItems(orderRequest.getOrderLineItems())
                .orderNumber(UUID.randomUUID().toString())
                .build();
        orderRepository.save(order);

        log.info("Sending Order Details to Notification Service");
        streamBridge.send("notificationEventSupplier-out-0", order.getId());

        return "Order Place Successfully";
    }
}
