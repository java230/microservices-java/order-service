package com.darosy.orderservice.client;

import org.springframework.cloud.client.circuitbreaker.NoFallbackAvailableException;

public class InventoryFallBack implements InventoryServiceClient {
    @Override
    public Boolean isInStock(String skuCode) {
        throw new NoFallbackAvailableException("Oops! Something went wrong, please order after some time!", new RuntimeException());
    }
}
