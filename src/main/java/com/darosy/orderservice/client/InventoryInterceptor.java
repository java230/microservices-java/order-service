package com.darosy.orderservice.client;

import com.darosy.orderservice.config.AuthenticationFacade;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;

public class InventoryInterceptor implements RequestInterceptor {

    @Autowired
    private AuthenticationFacade authenticationFacade;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header("Content-Type", ContentType.APPLICATION_JSON.getMimeType());
        requestTemplate.header("Accept", ContentType.APPLICATION_JSON.getMimeType());
        requestTemplate.header("Authorization", "Bearer " + authenticationFacade.getToken());
    }
}
