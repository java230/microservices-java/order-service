package com.darosy.orderservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "inventory-service", configuration = InventoryConfig.class)
public interface InventoryServiceClient {

    @GetMapping("/api/inventory")
    Boolean isInStock(@RequestParam String skuCode);
}
