package com.darosy.orderservice.client;

import org.springframework.context.annotation.Bean;

public class InventoryConfig {

    @Bean
    public InventoryInterceptor inventoryInterceptor() {
        return new InventoryInterceptor();
    }
}
