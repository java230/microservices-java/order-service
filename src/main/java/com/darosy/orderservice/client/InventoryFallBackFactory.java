package com.darosy.orderservice.client;

import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class InventoryFallBackFactory implements FallbackFactory<InventoryFallBack> {
    @Override
    public InventoryFallBack create(Throwable cause) {
        return new InventoryFallBack();
    }
}
